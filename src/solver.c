#include<stdio.h>
#include<stdlib.h>

#define BLK_X 3
#define BLK_Y 3
#define SIZE (BLK_X*BLK_Y)

#define LL long long int

int board [SIZE][SIZE];
int possible_x [SIZE][SIZE];
int possible_y [SIZE][SIZE];
int possible_block [BLK_X][BLK_Y][SIZE];
long long int solns = 0;

void print_solution (LL solution_no)
{
	int lx,ly;
	if (solution_no > 0)
		printf("\nSolution ID : %lld\n",solution_no);
	else
		printf("\n\n");
	for (lx = 0; lx < SIZE; lx++)
	{
		for (ly = 0; ly < SIZE; ly++)
		{
			printf("%2d ",board [lx][ly]);
		}
		printf("\n");
	}
}

void clear_all ()
{
	int lx,ly;
	for (lx = 0; lx < SIZE; lx++)
	{
		for (ly = 0; ly < SIZE; ly++)
		{
			board [lx][ly] = 0;
			possible_x [lx][ly] = 1;
			possible_y [lx][ly] = 1;
			possible_block [lx / BLK_Y][lx % BLK_Y][ly] = 1;
		}
	}
}

int insert_no (int locx, int locy, int no)
{
	--no;
	if (possible_x [locx][no] == 0 || possible_y [locy][no] == 0 || possible_block [locx / BLK_Y][locy / BLK_X][no] == 0)
	{
		return 1;
	}
	possible_x [locx][no] = 0;
	possible_y [locy][no] = 0;
	possible_block [locx / BLK_Y][locy / BLK_X][no] = 0;
	board [locx][locy] = no + 1;
	return 0;
}

void remove_no (int locx, int locy)
{
	int no = board [locx][locy];
	if (no == 0)
		return;
	--no;
	possible_x [locx][no] = 1;
	possible_y [locy][no] = 1;
	possible_block [locx / BLK_Y][locy / BLK_X][no] = 1;
	board [locx][locy] = 0;
}

void solve (int locx, int locy, int no, int no_filled)
{
	int lx, ly, l;
	if (no_filled == -1)
	{
		no_filled++;
		for (lx = 0; lx < SIZE; lx++)
			for (ly = 0; ly < SIZE; ly++)
				if (board [lx][ly] != 0)
					no_filled++;
	}
	if (no_filled == SIZE * SIZE)
	{
		print_solution (++solns);
		return;
	}
	if (locx == -1 || locy == -1)
	{
		for (lx = 0; lx < SIZE; lx++)
		{
			for (ly = 0; ly < SIZE; ly++)
			{
				if (board [lx][ly] == 0)
				{
					locx = lx;
					locy = ly;
					goto ENDLOC;
				}
			}
		}
	}
	ENDLOC:
	if (no == -1)
	{
		for (l = 0; l < SIZE; l++)
		{
			if (possible_x [locx][l] && possible_y [locy][l] && possible_block [locx / BLK_Y][locy / BLK_X][l])
			{
				solve (locx, locy, l + 1, no_filled); 
			}
		}
		return;
	}
	insert_no (locx, locy, no);
	solve (-1, -1, -1, no_filled + 1);
	remove_no (locx, locy);
}

void get ()
{
	int lx, ly;
	for (lx = 0; lx < SIZE; lx++)
	{
		for (ly = 0; ly < SIZE; ly++)
		{
			scanf ("%d", & board [lx][ly]);
		}
	}
}

void fill_all ()
{
	int lx, ly;
	for (lx = 0; lx < SIZE; lx++)
	{
		for (ly = 0; ly < SIZE; ly++)
		{
			if (board [lx][ly] != 0)
				insert_no (lx, ly, board [lx][ly]);
		}
	}
}

int main ()
{
	clear_all ();
	get ();
	fill_all ();
	solve (-1, -1, -1, -1);
	return 0;
}
